//Michael Obadia, 1937890
package LinearAlgebra;
public class Vector3d {
    private final double x;
    private final double y;
    private final double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
    }

    public double dotProduct(Vector3d v2) {
        return (this.x * v2.getX()) + (this.y * v2.getY()) + (this.z * v2.getZ());
    }

    public Vector3d add(Vector3d v2) {
        return new Vector3d((x + v2.getX()), (y + v2.getY()), (z + v2.getZ()));
    }
}