//Michael Obadia, 1937890
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class VectorTest {
    Vector3d v1 = new Vector3d(5, 7, 3);


    @Test
    public void testGetX() {
        assertEquals(v1.getX(),5);
    }

    @Test
    public void testGetY() {
        assertEquals(v1.getY(),7);
    }

    @Test
    public void testGetZ() {
        assertEquals(v1.getZ(),3);
    }

    @Test
    public void testMag() {
        assertEquals(v1.magnitude(), 9.1104335791443);
    }

    @Test
    public void testDot() {
        Vector3d v2 = new Vector3d(10, 3, 8);
        assertEquals(v1.dotProduct(v2), 95);
    }

    @Test
    public void testAddX() {
        Vector3d v2 = new Vector3d(10, 3, 8);
        assertEquals(v1.add(v2).getX(), 15);
    }
}
